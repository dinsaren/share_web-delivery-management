package com.dinsaren.webdeliverymanagement.controllers.auth;

import com.dinsaren.webdeliverymanagement.models.ERole;
import com.dinsaren.webdeliverymanagement.models.Role;
import com.dinsaren.webdeliverymanagement.models.User;
import com.dinsaren.webdeliverymanagement.payload.req.LoginReq;
import com.dinsaren.webdeliverymanagement.payload.req.SignupReq;
import com.dinsaren.webdeliverymanagement.payload.res.JwtRes;
import com.dinsaren.webdeliverymanagement.payload.res.MessageRes;
import com.dinsaren.webdeliverymanagement.repository.RoleRepository;
import com.dinsaren.webdeliverymanagement.repository.UserRepository;
import com.dinsaren.webdeliverymanagement.security.jwt.JwtUtils;
import com.dinsaren.webdeliverymanagement.security.services.impl.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser( @RequestBody LoginReq loginReq) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginReq.getUsername(), loginReq.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtRes(jwt,
                         userDetails.getId(), 
                         userDetails.getUsername(), 
                         userDetails.getEmail(), 
                         roles));
  }

  @PostMapping("/signup")
  public ResponseEntity<Object> userRegister(@RequestBody SignupReq signUpReq){
    if (userRepository.existsByUsername(signUpReq.getUsername())) {
      return ResponseEntity
              .badRequest()
              .body(new MessageRes("Error: Username is already taken!"));
    }

    if (userRepository.existsByEmail(signUpReq.getEmail())) {
      return ResponseEntity
              .badRequest()
              .body(new MessageRes("Error: Email is already in use!"));
    }
    User user = new User(signUpReq.getUsername(),
            signUpReq.getEmail(),
            encoder.encode(signUpReq.getPassword()));
    Set<Role> roles = new HashSet<>();
    Role userRole = roleRepository.findByName(ERole.ROLE_USER)
            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
    roles.add(userRole);
    user.setRoles(roles);
    userRepository.save(user);
    return ResponseEntity.ok(new MessageRes("User registered successfully!"));

  }

//  @PostMapping("/signup")
//  public ResponseEntity<?> registerUser(@RequestBody SignupReq signUpReq) {
//    if (userRepository.existsByUsername(signUpReq.getUsername())) {
//      return ResponseEntity
//          .badRequest()
//          .body(new MessageRes("Error: Username is already taken!"));
//    }
//
//    if (userRepository.existsByEmail(signUpReq.getEmail())) {
//      return ResponseEntity
//          .badRequest()
//          .body(new MessageRes("Error: Email is already in use!"));
//    }
//
//    // Create new user's account
//    User user = new User(signUpReq.getUsername(),
//               signUpReq.getEmail(),
//               encoder.encode(signUpReq.getPassword()));
//
//    Set<String> strRoles = signUpReq.getRole();
//    Set<Role> roles = new HashSet<>();
//
//    if (strRoles == null) {
//      Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//      roles.add(userRole);
//    } else {
//      strRoles.forEach(role -> {
//        switch (role) {
//        case "admin":
//          Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
//              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//          roles.add(adminRole);
//          break;
//        case "mod":
//          Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
//              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//          roles.add(modRole);
//          break;
//        default:
//          Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//          roles.add(userRole);
//        }
//      });
//    }
//    user.setRoles(roles);
//    userRepository.save(user);
//
//    return ResponseEntity.ok(new MessageRes("User registered successfully!"));
//  }

}
