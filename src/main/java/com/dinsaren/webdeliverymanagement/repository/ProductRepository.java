package com.dinsaren.webdeliverymanagement.repository;

import com.dinsaren.webdeliverymanagement.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAllByStatusAndCreatedBy(String status, Integer createdBy);
}
