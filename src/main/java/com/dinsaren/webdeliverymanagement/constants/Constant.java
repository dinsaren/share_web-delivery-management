package com.dinsaren.webdeliverymanagement.constants;

public class Constant {
    public static final String BEARER = "Bearer" ;
    String CIPHER_CRYPTO = "AES/CBC/PKCS5PADDING";
    public static final String STATUS_INIT = "INT";
    public static final String STATUS_ACTIVE = "ACT";
    public static final String STATUS_SUSPEND = "SUS";
    public static final String STATUS_LOCKED = "LCK";
    public static final String STATUS_BLOCK = "BLK";
    public static final String STATUS_DISABLE = "DBL";
    public static final String STATUS_DELETE = "DEL";
    public static final String STATUS_INACTIVE = "INA";
    public static final String STATUS_PENDING = "PEN";
    public static final String STATUS_COMPLETED = "COM";
    public static final String STATUS_REJECTED = "REJ";
    public static final Boolean STATUS_ENABLE_TRUE = Boolean.TRUE;

    public static final String STATUS_FALSE = "FALSE";
    public static final String STATUS_TRUE = "TRUE";
    public static final String STATUS_SUCCESSFUL = "TSC";
    public static final String STATUS_FAILED = "TFL";
    public static final String STATUS_REVERSAL_SUCCESSFUL = "RSC";
    public static final String STATUS_REVERSAL_FAILED = "RFL";


    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String EXP_COMMA = ",";


    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String CLIENT_SECRET_HEADER = "Client-Secret";
    public static final String CLIENT_SESSION_HEADER = "Session-Id";
    public static final String CLIENT_USER_ID = "User-Id";
    public static final String CLIENT_LANG = "User-Lang";
    public static final String USER_AGENT = "User-Agent";
    public static final String USER_REQUEST_FORWARD = "X-Forwarded-For";

    public static final String LANG = "LANG";
    public static final String LANG_KH = "KH";
    public static final String LANG_EN = "EN";
    public static final String LANG_CN = "CN";

    public static final String NOT_FOUND = "Not found";

    public static final String AES_PATTERN = "AES/CBC/PKCS5Padding";
    public static final String AES = "AES";
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    public static final String NAME = "NAME";
    public static final String END_URL = "/(.*)";
    public static final String START_URL = "^/";

    public static final String PHONE_PREFIX_KH = "+855";

    public static final String REGISTERED = "REGISTERED";
    public static final String NOT_REGISTERED = "NOT_REGISTERED";
    public static final String NEW_DEVICE = "NEW";
    public static final String OLD_DEVICE = "OLD";

    public static final String ROLE_CONTACT_MANAGER = "ROLE_CONTACT_MANAGER";
    public static final String TELEGRAM_CHANNEL = "TELEGRAM";

    public static final String CCY_KHR = "KHR";
    public static final String CCY_USD = "USD";

    public static final String TYPE_USER = "USER";
    public static final String SYS = "SYS";
    public static final String USER_ROLE = "USER_ROLE";

}
