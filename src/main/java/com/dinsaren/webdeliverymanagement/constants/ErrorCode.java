package com.dinsaren.webdeliverymanagement.constants;

public final class ErrorCode {

    public static final String SUCCESS = "SUC-000";
    public static final String GENERAL_ERROR = "ERR-000";
    public static final String BAD_REQUEST = "ERR-001";
    public static final String INVALID_USER_LOGIN_SESSION = "ERR-002";
    public static final String USER_REGISTER_ALREADY_EXIT = "ERR-003";
    public static final String INVALID_REQ_ERROR = "ERR-004";
    public static final String USER_NOT_FOUND = "ERR-005";

    public static final String INVALID_SIGN_ERROR = "ERR-083";


}
