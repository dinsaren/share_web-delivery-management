package com.dinsaren.webdeliverymanagement.payload.res;

import com.dinsaren.webdeliverymanagement.models.Role;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class UserRes {
  private Long id;
  private String username;
  private String email;
  private String password;
  private Integer refId;
  private String shipperName;
  private Set<Role> roles = new HashSet<>();
}
